#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cctype>
using namespace std;

class LeetCipher {
public:
	LeetCipher();
	string cipel(string& text);
private:
	vector<vector <string>> leetabc;
	vector<string> abc;
};

	LeetCipher::LeetCipher() {
	srand (time(NULL));
leetabc.resize(26);
leetabc[0]= {"4", "@", "/\\", "a"};
leetabc[1]={"8","|3", "13", "|}", "|8", "6", "|8", "b"};
leetabc[2]= { "<", "{", "[", "(", "c"};
leetabc[3] = {"|)", "|}", "|]", "d"};
leetabc[4] = {"3", "e"};
leetabc[5] = { "|=", "ph", "|#", "f"};
leetabc[6] = {"[", "[+", "6", "g"};
leetabc[7] = {"4", "|-|", "[-]", "h"};
leetabc[8] = {"1", "|", "!", "i"};
leetabc[9] = {"_|", "_/", "_7", "j"};
leetabc[10] = {"|<", "1<", "l<", "k"};
leetabc[11] = {"|_", "|", "1", "l"};
leetabc[12] = {"44", "(V)", "|\\/|", "m"};
leetabc[13] = {"/||/", "/V", "n"};
leetabc[14] ={"0", "()", "[]", "o"};
leetabc[15] = { "|o", "|O", "|>", "|D", "/o", "p"};
leetabc[16] = {"O_", "9", "(,)", "0,", "q"};
leetabc[17] = {"|2"," 12", "l2", "r"};
leetabc[18] = {"5", "$", "§", "s"};
leetabc[19] = {"7", "‘|‘", "t"};
leetabc[20] = {"|_|", "(_)", "[_]", "{_}", "u"};
leetabc[21] = {"\\/", "v"};
leetabc[22] = {"VV", "\\/\\/", "w"};
leetabc[23] = {"%", "><", "}{", ")(", "x"};
leetabc[24] = {"‘/", "y"};
leetabc[25] = { "2", "7_", ">_", "z"};

abc =
{
	"A" ,
	"B",
	"C",
	"D",
	"E",
	"F",
	"G",
	"H",
	"I",
	"J",
	"K",
	"L",
	"M",
	"N",
	"O",
	"P",
	"Q",
	"R",
	"S",
	"T",
	"U",
	"V",
	"W",
	"X",
	"Y",
	"Z"
};
}

string LeetCipher::cipel(string &text) {

	string leet = "";
	string be = text;

	transform(be.begin(), be.end(),be.begin(), ::toupper);

	while(!be.empty())
	{
	bool find=false;
	for (int i=0; i<abc.size(); i++)
		{	
		size_t found = be.find(abc[i]);
		if (found==0)
			{
			find =true;
			leet+=leetabc[i][rand() % leetabc[i].size()];
			if(be.length() > 1)
			be=be.substr(1);
		else
			be.clear();
		}
	}

	if(!find){
		leet+=be[0];
		if(be.length()>1)
			be=be.substr(1);
		else
			be.clear();
		}
	}

return leet;
}

int main(int argc, char * argv[])
{
	string bemenet = "";
	
	std::cin>>bemenet;
	
	LeetCipher* cipher = new LeetCipher();
	string cipeltBemenet = cipher->cipel(bemenet);
	std::cout<<cipeltBemenet<<endl;
}
