privileged aspect Binfa{

    public pointcut atlag(): call(public double getAtlag ());

    after(): atlag()
    {
        System.out.println("--- Átlagszámítás ---");
    }
}