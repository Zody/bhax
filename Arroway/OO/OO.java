import java.math.*;


public class OO{
    public static void main(String [] args){
        
        Object teszt = new Object();   
        
        for(int i = 0; i < 10; i++){
            System.out.println(teszt.getVal());
        }
    }
}
class Object{
    private boolean existance;
    private double val;
    
    public Object(){
        this.existance=false;
    }
    
    public double getVal(){
        if (!existance)
        {
		double u1, u2, v1, v2, w;
        
        //FerSML Project ből másolva a számítás
		do{
			u1 = (double)(Math.random()*10); 
			u2 = (double)(Math.random()*10);
			v1 = 2 * u1 - 1;
			v2 = 2 * u2 - 1;
			w = v1 * v1 + v2 * v2;
		    }
		while (w > 1);

		double r = Math.sqrt ((-2 * Math.log (w)) / w);

		this.val = r * v2; 
		this.existance = !existance;

		return r * v1;
	}
	
        else   //Az if - else ága
        {
            existance = !existance; //amennyiben már volt eltárolt adat akkor azt adja vissza a program
            System.out.println("Already exists, no need to create new.");
            return val;
        }
    }
    
}
