#include <iostream>

class Int{
private: int* value;
public: 
    Int(int x){
	value= new int(x);
    std::cout<<"constr"<<std::endl;
    }

    ~Int(){
    delete value;
    std::cout<<"destr"<<std::endl;
    }
    
    Int (const Int & x) : value (new int) {
        *value=*(x.value);
        std::cout<<"copy ctor"<<std::endl;
    }
    
    Int& operator= (const Int & n){
        int *new_value = new int();
        *new_value = *(n.value);
        delete value;
        value=new_value;
        std::cout<<"copy assign"<<std::endl;
        return *this;
    }
    
    Int (Int && n) : value (nullptr){
        *this = std::move(n);
        std::cout<<"move ctor"<<std::endl;
    }
    
    Int& operator= (Int && n){
        std::swap(value,n.value);
        std::cout<<"move assign"<<std::endl;
        
        return *this;
    }
    
 
};

int main(){
    Int k(6);
    Int j(5);
    Int l(k);
    j=k;
    l=std::move(k);
    Int m=std::move(j);
    
}
