package model;


/**
* @generated
*/
public class SzemélyGépkocsi extends Jármű {
    
    /**
    * @generated
    */
    private String AjtókSzáma;
    
    /**
    * @generated
    */
    private Integer Lökettérfogat;
    
    /**
    * @generated
    */
    private String Szín;
    
    /**
    * @generated
    */
    private Integer SzemélyekSzáma;
    
    
    
    /**
    * @generated
    */
    public String getAjtókSzáma() {
        return this.AjtókSzáma;
    }
    
    /**
    * @generated
    */
    public String setAjtókSzáma(String AjtókSzáma) {
        this.AjtókSzáma = AjtókSzáma;
    }
    
    /**
    * @generated
    */
    public Integer getLökettérfogat() {
        return this.Lökettérfogat;
    }
    
    /**
    * @generated
    */
    public Integer setLökettérfogat(Integer Lökettérfogat) {
        this.Lökettérfogat = Lökettérfogat;
    }
    
    /**
    * @generated
    */
    public String getSzín() {
        return this.Szín;
    }
    
    /**
    * @generated
    */
    public String setSzín(String Szín) {
        this.Szín = Szín;
    }
    
    /**
    * @generated
    */
    public Integer getSzemélyekSzáma() {
        return this.SzemélyekSzáma;
    }
    
    /**
    * @generated
    */
    public Integer setSzemélyekSzáma(Integer SzemélyekSzáma) {
        this.SzemélyekSzáma = SzemélyekSzáma;
    }
    
}

