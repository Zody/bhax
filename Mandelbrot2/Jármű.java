package model;


/**
* @generated
*/
public class Jármű {
    
    /**
    * @generated
    */
    private String rendszám;
    
    /**
    * @generated
    */
    private String alvázszám;
    
    /**
    * @generated
    */
    private String tulaj;
    
    /**
    * @generated
    */
    private Integer forgalomba_helyezve;
    
    
    
    /**
    * @generated
    */
    public String getRendszám() {
        return this.rendszám;
    }
    
    /**
    * @generated
    */
    public String setRendszám(String rendszám) {
        this.rendszám = rendszám;
    }
    
    /**
    * @generated
    */
    public String getAlvázszám() {
        return this.alvázszám;
    }
    
    /**
    * @generated
    */
    public String setAlvázszám(String alvázszám) {
        this.alvázszám = alvázszám;
    }
    
    /**
    * @generated
    */
    public String getTulaj() {
        return this.tulaj;
    }
    
    /**
    * @generated
    */
    public String setTulaj(String tulaj) {
        this.tulaj = tulaj;
    }
    
    /**
    * @generated
    */
    public Integer getForgalomba_helyezve() {
        return this.forgalomba_helyezve;
    }
    
    /**
    * @generated
    */
    public Integer setForgalomba_helyezve(Integer forgalomba_helyezve) {
        this.forgalomba_helyezve = forgalomba_helyezve;
    }
    

    //                          Operations                                  
    
    
}

