#include <iostream>
#include <math.h>
#include <time.h>
long
n16modk(int n, int k)
{
		int t = 1;
		while(t <= n)
		{
			t =t*2;
		}
		long r = 1;
		while(true)
		{
			if(n >= t)
			{
				r = (16*r) % k;
				n = n - t;
			}
			t = t/2;
			if(t < 1)
			{
				break;
			}
			r = (r*r) % k;
		}
		return r;
}
double
d16Sj(int d, int j)
{
		double szam = 0.0d;
		for(int i=0;i<=d;++i)
		{
			szam += n16modk(d-i, 8*i+j) / (double)(8*i+j);
		}
		for(int i=d+1;i<=2*d;++i)
		{
			szam += pow(16.0d ,d-i) / (double)(8*i+j);
		}
		return szam - floor(szam);
}
int main()
{
    int d=100000000;
		
	double d16pi = 0.0;

	clock_t delta = clock();
			
	double d16S1 = d16Sj(d,1);
	double d16S4 = d16Sj(d,4);
	double d16S5 = d16Sj(d,5);
	double d16S6 = d16Sj(d,6);
		
	d16pi = (4.0*d16S1 - 2.0*d16S4 - d16S5 - d16S6) - floor(4.0*d16S1 - 2.0*d16S4 - d16S5 - d16S6);

    d16pi = d16pi - floor(d16pi);
		
	int szjegy = (int)floor(d16pi * 16.0);

    //std::cout<<szjegy<<std::endl;
	std::cout<<"Hexa-jegyek száma: 10^8"<<std::endl;
	delta = clock() - delta;
	std::cout<<(double)delta/CLOCKS_PER_SEC<<std::endl;
}
]]></programlisting>
        </para>
        <para>
            És következnek az eredmények. A pi 10^6., 10^7. majd 10^8. számjegyének kiszámításakor.
        </para>
        
        <para>
            A C# kódhoz kis kiegészítés. A fordításhoz és a futtatáshoz szükséges a <command>dotnet</command> program. Ezt feltelepítve 
            létre kell hozni egy projektet, majd abba kell beletenni a C# forráskódot, aztán a <command>dotnet</command> paranccsal már 
            pöröghet is a programunk.
        </para>
        <para>
            <programlisting language="c"><![CDATA[public class PiBBPBench {
    public static double d16Sj(int d, int j) {
        
        double d16Sj = 0.0d;
        
        for(int k=0; k<=d; ++k)
            d16Sj += (double)n16modk(d-k, 8*k + j) / (double)(8*k + j);
        
        return d16Sj - System.Math.Floor(d16Sj);
    }

    public static long n16modk(int n, int k) {
        
        int t = 1;
        while(t <= n)
            t *= 2;
        
        long r = 1;
        
        while(true) {
            
            if(n >= t) {
                r = (16*r) % k;
                n = n - t;
            }
            
            t = t/2;
            
            if(t < 1)
                break;
            
            r = (r*r) % k;
            
        }
        
        return r;
    }

     public static void Main(System.String[]args) { 
        
        double d16Pi = 0.0d;
        
        double d16S1t = 0.0d;
        double d16S4t = 0.0d;
        double d16S5t = 0.0d;
        double d16S6t = 0.0d;
        
        int jegy = 0;
        
        System.DateTime kezd = System.DateTime.Now;
        
        for(int d=1000000; d<1000001; ++d) {
            
            d16Pi = 0.0d;
            
            d16S1t = d16Sj(d, 1);
            d16S4t = d16Sj(d, 4);
            d16S5t = d16Sj(d, 5);
            d16S6t = d16Sj(d, 6);
            
            d16Pi = 4.0d*d16S1t - 2.0d*d16S4t - d16S5t - d16S6t;
            
            d16Pi = d16Pi - System.Math.Floor(d16Pi);
            
            jegy = (int)System.Math.Floor(16.0d*d16Pi);
            
        }
        
        System.Console.WriteLine(jegy);
        System.TimeSpan delta = System.DateTime.Now.Subtract(kezd);
        System.Console.WriteLine(delta.TotalMilliseconds/1000.0);
    }
