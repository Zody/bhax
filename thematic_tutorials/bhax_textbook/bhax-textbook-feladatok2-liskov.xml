<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Liskov!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

    <section>
        <title> A Liskov helyettesítés sértése</title>
        <para>
            Írjunk olyan OO, leforduló Java és C++ kódcsipetet, amely megsérti a Liskov elvet! Mutassunk rá a megoldásra: jobb OO tervezés.
	</para>
	<para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/Zody/bhax/tree/master/Liskov">gitlab.com/Zody/bhax/tree/master/Liskov</link>             
        </para>
        <para>
A Liskov-elvről bővebben: https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_1.pdf
(számos példa szerepel az elv megsértésére az UDPROG repóban, lásd pl. source/binom/BatfaiBarki/madarak/)
        </para>
	<para>
A program:
        </para>
<programlisting language="Java"><![CDATA[
class Madar {
public
     void repul() {System.out.println("repul");};
}

class Program {
public
     void fgv ( Madar madar ) {
          madar.repul();
     }
}


class Sas extends Madar
{}

class Pingvin extends Madar
{}

class Liskov{

public static void main ( String[] args)
{
     Program program= new Program();
     Madar madar=new Madar();
     program.fgv ( madar );

     Sas sas=new Sas();
     program.fgv ( sas );

     Pingvin pingvin=new Pingvin();
     program.fgv ( pingvin ); 
}
}
]]></programlisting>
        <para>
Mit csinál a program? Van egy Madár osztály, aki tud repülni. Van egy Program osztály, a Program meg tudja reptetni a madarat. A Madár ősosztálnyak van két gyermeke: a Sas és a Pingvin. A mainben példányosítunk egy programot, egy madarat, és a madár repül. Ezután létrehozunk egy sast, aki szintén repül, majd egy pingvint, aki szintén repül.
        </para>
<screen>
benedek@benedek-VirtualBox:~/bhax/thematic_tutorials/bhax_textbook/Liskov$ javac Liskov.java 
benedek@benedek-VirtualBox:~/bhax/thematic_tutorials/bhax_textbook/Liskov$ java Liskov
repul
repul
repul
</screen>
        <para>
De mi ezzel a baj? Hát az, hogy a pingvin repül! Megoldás: úgy kellene megszervezni az osztályokat, hogy a pingvin ne tudjon repülni. A fenti kód C++ nyelven is megtalálható a repómban, de nézzünk most egy megoldást erre a programra, C++ nyelven:
        </para>
<programlisting language="C++"><![CDATA[
class Madar {
};

class Program {
public:
     void fgv ( Madar &madar ) {
     }
};

class RepuloMadar : public Madar {
public:
     virtual void repul() {};
};

class Sas : public RepuloMadar
{};

class Pingvin : public Madar
{};

int main ( int argc, char **argv )
{
     Program program;
     Madar madar;
     program.fgv ( madar );

     Sas sas;
     program.fgv ( sas );

     Pingvin pingvin;
     program.fgv ( pingvin );

}
]]></programlisting>
        <para>
A különbség: attól, hogy valami madár, nem biztos, hogy tud repülni. A repul() metódust tehát nem minden madárnak kell tudnia. Erre szolgál a Repulomadar osztály, ami a Madar alosztálya, tehát mindent tud, amit egy Madar, plusz vannak sajátosságai, itt a repülés. Tehát a repul() metódust csak azok a madarak tudják használni, amelyek repülőmadarak. A Sas osztály ide tartozik, de a pingvin nem. A virtual kulcsszó miatt a kötés dinamikus lesz (Javaban mindig az).
        </para>

    </section>        

    <section>
        <title>Szülő-gyerek</title>
        <para>
Írjunk Szülő-gyerek Java és C++ osztálydefiníciót, amelyben demonstrálni tudjuk, hogy az ősön
keresztül csak az ős üzenetei küldhetőek!
https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_1.pdf (98. fólia)
        </para>
        <para>
            Megoldás videó: <link xlink:href=""></link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/Zody/bhax/tree/master/Liskov">gitlab.com/Zody/bhax/tree/master/Liskov</link>             
        </para>
        <para>
            Az operációs rendszer mutatóin és referenciáin keresztül csak a rendszer függvényeit tudjuk alkalmazni:
        </para>
	<para>
		C++:
	</para>
	<programlisting><![CDATA[
#include <iostream>
using namespace std;
class Vehicles{
public:
//    void setNumberOfWheels(){
//        wheels = 0;
//    }
    int wheels = 0;
};
class Cars: public Vehicles{
public:
    void setNumberOfWheels(){
        wheels = 4;
    }
};
int main()
{
    Vehicles &vehicle = *new Cars();
    vehicle.setNumberOfWheels(); //Hibás, mivel az õs nem tudja elérni a gyermek metódusait.
    cout << "The number of wheels: " << vehicle.wheels << endl;
}
            ]]></programlisting>
	<para>
		Létezik a <varname>Vehicles</varname> class, melynek értelem szerűen a <varname>wheels</varname> nevű változója a kerekek számát tárolja, melynek alapértéke 0.
	</para>
	<para>
		A <varname>Cars</varname> classban van egy <function>setNumberOfWheels()</function> fgv. ami ezt az értéket 4 -re állítja majd.
	</para>
	<para>
		A <function>main</function> -ben létrehozunk egy <varname>Vehicles</varname> referenciát, aminek az értékül egy <varname>Cars</varname> referencia lesz adva. Ha a 
            <function>vehicle</function> referncián keresztül meghívjuk a <function>setNumberOfWheels</function>
            függvényt, akkor hibát dob a program, nem találja a függvény definícióját.
	</para>
	<para>
            Java:
            <programlisting language="java"><![CDATA[
public class Vehicles{
//	public void setNumberOfWheels(){
//		wheels = 0;
//	}
	public int wheels;
	public static class Cars extends Vehicles{
	    	public void setNumberOfWheels(){
		        wheels = 4;
	    	}
	}
	public static void main(String[] args)
	{
    		Vehicles vehicle = new Cars();
		vehicle.setNumberOfWheels(); //Hibás, mert az õs a leszármazott metódusait nem éri el.
		System.out.println("The number of wheels: "+vehicle.wheels);
	}
}
            ]]></programlisting>
        </para>
    </section>  

    <section>
        <title>Anti OO</title>
        <para>
            A BBP algoritmussal 4 a Pi hexadecimális kifejtésének a 0. pozíciótól számított 10 6, 107, 108 darab
jegyét határozzuk meg C, C++, Java és C# nyelveken és vessük össze a futási időket!
            <link xlink:href="https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/apas03.html#id561066">https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/apas03.html#id561066</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/Zody/bhax/tree/master/Liskov">gitlab.com/Zody/bhax/tree/master/Liskov</link>             
        </para>
        <para>
            Az előző fejezetben látott és meg is írt BBP algoritmussal kell foglalkozzunk. Most más nyelvekre fogjuk átírni, és a végrehajtás 
            sebessége szerint hasonlítjuk őket össze.
        </para>
        <para>
            Java kód:
            <programlisting language="java"><![CDATA[
import java.lang.*;
import java.util.*;

class bbp
{
	public static long n16modk(int n, int k)
	{
		int t = 1;
		while(t <= n)
		{
			t =t*2;
		}
		long r = 1;
		while(true)
		{
			if(n >= t)
			{
				r = (16*r) % k;
				n = n - t;
			}
			t = t/2;
			if(t < 1)
			{
				break;
			}
			r = (r*r) % k;
		}
		return r;
	}
	public static double d16Sj(int d, int j)
	{
		double szam = 0.0d;
		for(int i=0;i<=d;++i)
		{
			szam += n16modk(d-i, 8*i+j) / (double)(8*i+j);
		}
		for(int i=d+1;i<=2*d;++i)
		{
			szam += StrictMath.pow(16.0d ,d-i) / (double)(8*i+j);
		}
		return szam - StrictMath.floor(szam);
	}
	
	public static void main(String args[])
	{
		int d=100000000;
		StringBuffer buffer = new StringBuffer();
		
		Character hexa[] = {'A','B','C','D','E','F'};
		
		double d16pi;

		long delta = System.currentTimeMillis();
			
		double d16S1 = d16Sj(d,1);
		double d16S4 = d16Sj(d,4);
		double d16S5 = d16Sj(d,5);
		double d16S6 = d16Sj(d,6);
			
		d16pi = (4.0d*d16S1 - 2.0d*d16S4 - d16S5 - d16S6) - StrictMath.floor(4.0d*d16S1 - 2.0d*d16S4 - d16S5 - d16S6);
			
		while(d16pi != 0.0d)
		{
			int szjegy = (int)StrictMath.floor(d16pi * 16.0d);
			
			if(szjegy<10)
			{
				buffer.append(szjegy);
			}
			if(szjegy>=10)
			{
				buffer.append(hexa[szjegy-10]);
			}
			
			d16pi = (d16pi * 16.0d) - StrictMath.floor(d16pi * 16.0d);
		}	

		System.out.println(buffer);
		System.out.println("Hexa-jegyek száma: 10^8");
		delta = System.currentTimeMillis() - delta;
		System.out.println(delta/1000.0);					
	}
}
]]></programlisting>
        </para>
        <para>
            És következnek az eredmények. A pi 10^6., 10^7. majd 10^8. számjegyének kiszámításakor.
        </para>
        
        <para>
            Majd az algoritmus c++-beli implementációja.
            <programlisting language="java"><![CDATA[
#include <iostream>
#include <math.h>
#include <time.h>
long
n16modk(int n, int k)
{
		int t = 1;
		while(t <= n)
		{
			t =t*2;
		}
		long r = 1;
		while(true)
		{
			if(n >= t)
			{
				r = (16*r) % k;
				n = n - t;
			}
			t = t/2;
			if(t < 1)
			{
				break;
			}
			r = (r*r) % k;
		}
		return r;
}
double
d16Sj(int d, int j)
{
		double szam = 0.0d;
		for(int i=0;i<=d;++i)
		{
			szam += n16modk(d-i, 8*i+j) / (double)(8*i+j);
		}
		for(int i=d+1;i<=2*d;++i)
		{
			szam += pow(16.0d ,d-i) / (double)(8*i+j);
		}
		return szam - floor(szam);
}
int main()
{
    int d=100000000;
		
	double d16pi = 0.0;

	clock_t delta = clock();
			
	double d16S1 = d16Sj(d,1);
	double d16S4 = d16Sj(d,4);
	double d16S5 = d16Sj(d,5);
	double d16S6 = d16Sj(d,6);
		
	d16pi = (4.0*d16S1 - 2.0*d16S4 - d16S5 - d16S6) - floor(4.0*d16S1 - 2.0*d16S4 - d16S5 - d16S6);

    d16pi = d16pi - floor(d16pi);
		
	int szjegy = (int)floor(d16pi * 16.0);

    //std::cout<<szjegy<<std::endl;
	std::cout<<"Hexa-jegyek száma: 10^8"<<std::endl;
	delta = clock() - delta;
	std::cout<<(double)delta/CLOCKS_PER_SEC<<std::endl;
}
]]></programlisting>
        </para>
        <para>
            És következnek az eredmények. A pi 10^6., 10^7. majd 10^8. számjegyének kiszámításakor.
        </para>
        
        <para>
            A C# kódhoz kis kiegészítés. A fordításhoz és a futtatáshoz szükséges a <command>dotnet</command> program. Ezt feltelepítve 
            létre kell hozni egy projektet, majd abba kell beletenni a C# forráskódot, aztán a <command>dotnet</command> paranccsal már 
            pöröghet is a programunk.
        </para>
        <para>
            <programlisting language="c"><![CDATA[public class PiBBPBench {
    public static double d16Sj(int d, int j) {
        
        double d16Sj = 0.0d;
        
        for(int k=0; k<=d; ++k)
            d16Sj += (double)n16modk(d-k, 8*k + j) / (double)(8*k + j);
        
        return d16Sj - System.Math.Floor(d16Sj);
    }

    public static long n16modk(int n, int k) {
        
        int t = 1;
        while(t <= n)
            t *= 2;
        
        long r = 1;
        
        while(true) {
            
            if(n >= t) {
                r = (16*r) % k;
                n = n - t;
            }
            
            t = t/2;
            
            if(t < 1)
                break;
            
            r = (r*r) % k;
            
        }
        
        return r;
    }

     public static void Main(System.String[]args) { 
        
        double d16Pi = 0.0d;
        
        double d16S1t = 0.0d;
        double d16S4t = 0.0d;
        double d16S5t = 0.0d;
        double d16S6t = 0.0d;
        
        int jegy = 0;
        
        System.DateTime kezd = System.DateTime.Now;
        
        for(int d=1000000; d<1000001; ++d) {
            
            d16Pi = 0.0d;
            
            d16S1t = d16Sj(d, 1);
            d16S4t = d16Sj(d, 4);
            d16S5t = d16Sj(d, 5);
            d16S6t = d16Sj(d, 6);
            
            d16Pi = 4.0d*d16S1t - 2.0d*d16S4t - d16S5t - d16S6t;
            
            d16Pi = d16Pi - System.Math.Floor(d16Pi);
            
            jegy = (int)System.Math.Floor(16.0d*d16Pi);
            
        }
        
        System.Console.WriteLine(jegy);
        System.TimeSpan delta = System.DateTime.Now.Subtract(kezd);
        System.Console.WriteLine(delta.TotalMilliseconds/1000.0);
    }
}]]></programlisting>
        </para>
         <para>
            És végül az eredmények. A pi 10^6., 10^7. majd 10^8. számjegyének kiszámításakor.
        </para>
       
        <para>
            A kisebb szám kiszámításakor nem tapasztalunk jelentős eltéréseket, azonabn a 10^8. értéknél már látszik a különbség. 
            A legrövidebb futási idővel a java kód büszkélkedhet, a leglassabbéval pedig a c++ kód szomorkodhat.
        </para>
    </section>


    <section>
        <title>Hello, Android!</title>
        <para>
Élesszük fel az SMNIST for Humans projektet!
https://gitlab.com/nbatfai/smnist/tree/master/forHumans/SMNISTforHumansExp3/app/src/main
Apró módosításokat eszközölj benne, pl. színvilág.
        </para>
        <para>
            Megoldás videó: <link xlink:href=""></link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href=""></link>             
        </para>
        <para>
            
        </para>
    </section>  

    <section>
        <title>Ciklomatikus komplexitás</title>
        <para>
Számoljuk ki valamelyik programunk függvényeinek ciklomatikus komplexitását! Lásd a fogalom
tekintetében a https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_2.pdf (77-79
fóliát)!
        </para>
        <para>
            Megoldás videó: <link xlink:href=""></link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href=""></link>             
        </para>
        <para>
            
        </para>
    </section>     

	  
        
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                
